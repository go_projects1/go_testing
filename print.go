package benching

import (
	"fmt"
	"math/rand"
	"strconv"
)

func sprint(n int) {
	for i := 0; i < n; i++ {
		fmt.Sprint(rand.Int())
	}
}

func itoa(n int) {
	for i := 0; i < n; i++ {
		strconv.Itoa(rand.Int())
	}
}
