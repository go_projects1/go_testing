# Go Testing
```bash
simple tests

go test :  run testing
go test -v : run testing with more info
go test TestDevide.go : run testing for only one go file

bench tests

go test -bench=. : all which are starting with benchmark
go test -bench=BenchmarkSprint : only specific
go test -bench=. -benchmem : will we showed additionally(allocate, CPU)
go test -bench=. -benchmem -benchtime=1000x - X count loop
go test -bench=. -benchmem -benchtime=10s - with sekund duration

go test ./... -cover : statistics for covering tests
go test ./... -coverprofile=coverage.txt : saving statistics to the file 
go tool cover -html coverage.txt -o index.html : saving statistics to the html file 
```
