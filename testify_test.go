package benching

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

func divede(num1, num2 int) (int, error) {
	if num2 == 0 {
		return 0, errors.New("number 2 cannot zero")
	}
	return num1 / num2, nil
}

func TestDevide(t *testing.T) {
	num1 := 4
	num2 := 2
	expected := 2
	actual, err := divede(num1, num2)

	if err != nil {
		t.Errorf("Should not produce an error")
	}

	if expected != actual {
		t.Errorf("Result was incorrect, got: %d, want: %d.", actual, expected)
	}
}

func TestDevideTestify(t *testing.T) {
	num1 := 4
	num2 := 2
	expected := 2
	actual, err := divede(num1, num2)

	assert.Nil(t, err)
	assert.Equal(t, expected, actual)
}
