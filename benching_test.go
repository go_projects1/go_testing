package benching

import (
	"testing"
)

// go test -bench=. : all which are starting with go_testing
// go test -bench=BenchmarkSprint : only specific
// go test -bench=. -benchmem : will we showed additionally(allocate, CPU)
// go test -bench=. -benchmem -benchtime=1000x - X count loop
// go test -bench=. -benchmem -benchtime=10s - with sekund duration
func BenchmarkSimplest(b *testing.B) {

}

func BenchmarkSprint(b *testing.B) {
	sprint(b.N)
}

func BenchmarkItoa(b *testing.B) {
	itoa(b.N)
}
